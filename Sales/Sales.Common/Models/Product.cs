﻿namespace Sales.Common.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Product
    {
        #region Properties
        [Key]
        public int ProductId { get; set; }
        [Required]
        public string Descripction { get; set; }

        public Decimal Price { get; set; }

        public bool IsAvailable { get; set; }

        public DateTime  PublishOn { get; set; }
        #endregion
    }
}
