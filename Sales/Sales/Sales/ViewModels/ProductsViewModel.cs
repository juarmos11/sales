﻿namespace Sales.ViewModels
{
    using Lands.ViewModels;
    using Common.Models;
    using Services;
    using System.Collections.ObjectModel;
    using System;
    using Xamarin.Forms;
    using System.Collections.Generic;
    public class ProductsViewModel : BaseViewModel
    {
        #region Atributes
        private ApiService apiService;
        private ObservableCollection<Product> products; 
        #endregion

        #region Properties
        public ObservableCollection<Product> Products 
            {
                get {return this.products;}
                set { this.SetValue(ref this.products, value);}
             }
        #endregion
         
        #region Constructors
        public ProductsViewModel()
        {
            this.apiService = new ApiService();
            this.LoadProducts();
        }
        #endregion

        #region Methods
        private async void LoadProducts()
        {
            var response = await apiService.GetListAsync<Product>(
               //https://salesapiservices.azurewebsites.net,-->
                "http://localhost:53783",
                "/api", 
                "/Products"
                );
            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;
            }
            var list = (List<Product>)response.Result;
            this.Products = new ObservableCollection<Product>(list);
        }
        #endregion
    }
}
