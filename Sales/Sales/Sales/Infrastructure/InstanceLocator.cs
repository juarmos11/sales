﻿namespace Sales.Infrastructure
{
    using ViewModels;
    public class InstanceLocator
    {
        #region Properties
        public MainViewModel Main {get; set;} 
        #endregion

        #region Contrusctors
        public InstanceLocator()
        {
            Main = new MainViewModel();
        } 
        #endregion
    }
}
