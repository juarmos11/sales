USE [sales]
GO

/****** Object:  Table [dbo].[Census]    Script Date: 27/09/2018 19:34:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Census](
	[CensusId] [int] IDENTITY(1,1) NOT NULL,
	[Address] [nvarchar](max) NOT NULL,
	[Phone] [nvarchar](max) NULL,
	[People] [int] NOT NULL,
	[Stratum] [int] NOT NULL,
	[HaveInternet] [bit] NOT NULL,
	[HaveAqueduct] [bit] NOT NULL,
	[HaveElectricity] [bit] NOT NULL,
	[Observations] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[ImageArray] [varbinary](max) NULL,
 CONSTRAINT [PK_dbo.Census] PRIMARY KEY CLUSTERED 
(
	[CensusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

