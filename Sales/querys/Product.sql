USE [sales]
GO

/****** Object:  Table [dbo].[Product]    Script Date: 27/09/2018 19:35:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Product](
	[ProductId] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[IsAvailable] [bit] NOT NULL,
	[PublishOn] [datetime] NOT NULL
)

GO

