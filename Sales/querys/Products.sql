USE [sales]
GO

/****** Object:  Table [dbo].[Products]    Script Date: 27/09/2018 19:35:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[IsAvailable] [bit] NOT NULL,
	[PublishOn] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.Products] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

